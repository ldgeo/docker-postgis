# Docker image for PostgreSQL 12 / PostGIS 3 

Example command to run container:

	docker run \
	    -v $(pwd):/var/lib/postgresql/12/main/ \
	    -e POSTGRES_DB="osm" \
	    -e POSTGRES_USER="osm" \
	    -e POSTGRES_PASSWORD="osm" \
	    -e POSTGRES_PORT="5432" \
	    -d registry.gitlab.com/ldgeo/docker-postgis
