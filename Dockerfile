FROM postgres:12.1
LABEL maintainer="Ludovic Delauné <message@cartodev.com>"

ENV POSTGIS_VERSION 3.1.0alpha1
ENV POSTGIS_SHA256 a027358ffd691ddc32e6c9db3d9d4cfc030c95d0c3f172388cee4a60d7e7e270

RUN apt-get update \
    && apt-get install --no-install-recommends -y  wget ca-certificates \
    && wget -O postgis.tar.gz "https://github.com/postgis/postgis/archive/$POSTGIS_VERSION.tar.gz" \
    && echo "$POSTGIS_SHA256 *postgis.tar.gz" | sha256sum -c - \
    && apt-get install --no-install-recommends -y \
        openssl \
        build-essential \
        automake \
        autoconf \ 
        libtool \
        bison \
        libxml2-dev \
        libxml2-utils \
        libgeos-dev \
        libgdal-dev \
        libprotobuf-c-dev \
        protobuf-c-compiler \
        libjson-c-dev \
        xsltproc \
        wget \
        git \
        postgresql-server-dev-12 \
    && mkdir -p /usr/src/postgis \
    && tar \
        --extract \
        --file postgis.tar.gz \
        --directory /usr/src/postgis \
        --strip-components 1 \
    && rm postgis.tar.gz \
    && cd /usr/src/postgis \
    && ./autogen.sh \
    && make \
    && make install \
    && rm -rf /usr/src/postgis \
    && rm -rf /var/lib/apt/lists/*

COPY ./initdb-postgis.sh /docker-entrypoint-initdb.d/postgis.sh
COPY ./update-postgis.sh /usr/local/bin
